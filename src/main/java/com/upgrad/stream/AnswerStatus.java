package com.upgrad.stream;

public enum AnswerStatus {
    pending,
    graded
}
