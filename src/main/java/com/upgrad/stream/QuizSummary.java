package com.upgrad.stream;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class QuizSummary {
    private List<QuestionChoice> correctAnswers;
    private long allCorrectCount;
    private long partiallyCorrectCount;
    private long incorrectCount;
    private long totalSubmissions;
    private double numericCorrectAnswer;
    private QuestionType questionType;
}