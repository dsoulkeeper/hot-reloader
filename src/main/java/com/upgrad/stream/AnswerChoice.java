package com.upgrad.stream;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@EqualsAndHashCode(of = "questionChoiceId")
public class AnswerChoice {

    @JsonProperty("id")
    private Long questionChoiceId;

    private boolean selected;
    private boolean correct;
    private String feedback;
}
