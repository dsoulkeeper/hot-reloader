package com.upgrad.stream;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ChoiceResponseWrapper {
    private List<AnswerChoice> choices;
}
