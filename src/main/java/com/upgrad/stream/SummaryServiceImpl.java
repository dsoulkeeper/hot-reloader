package com.upgrad.stream;

import java.util.ArrayList;
import java.util.List;

/**
 * Objective of this problem is to improvise our stream API usage and develop the sense of functional programming.
 * Following code addresses one of the requirements in live sessions service.
 * QuestionSession object represents question response we get for a user from assessment service.
 * <p>
 * You will find all kinds of antipatterns here starting with
 * 1. Long method
 * 2. Duplicated code
 * 3. Non extensible
 * 4. Non readable
 * 5. Too many dangling variables
 * 6. Qualifies for the famous comment "When I wrote this, only God and I understood what I was doing. Now, God only knows"
 * <p>
 * Task:
 * 1. Refactor the code so that its more manageable
 * 2. Has no duplicate code
 * 3. There is no single imperative style for loop in code. Meaning, no "for (Object o: objects)" or "for (int i; i < something; i++)" types loops.
 * 4. Don't hack by replacing for loops with do..while or while loops
 * 5. SummaryServiceImplTest must pass without having to touch that class
 */
public class SummaryServiceImpl {

    // Currently there is no direct way to get correct choices for a quiz. So we have to deduce it.
    // First approach is to look for questionSession which is marked as correct. That mean its chosen answers are the correct choices
    // Otherwise, iterate over all questionSession's choices and filter choices that have selected and correct flag true
    public QuizSummary summarize(List<QuestionSession> questionSessions) {
        if (questionSessions.isEmpty()) {
            throw new RuntimeException("Insufficient data to generate summary.");
        }
        // Strategy 1 to get deduce right questionSessionChoices
        List<AnswerChoice> answerChoices = null;

        for (QuestionSession questionSession : questionSessions) {
            if (questionSession.isCorrect()) {
                answerChoices = questionSession.getChoiceResponse().getChoices();
                break;
            }
        }

        // That means there were no correct answers
        // We got to go with strategy 2 to find correct answers
        if (answerChoices == null) {
            answerChoices = new ArrayList<>();
            for (QuestionSession questionSession : questionSessions) {
                List<AnswerChoice> choices = questionSession.getChoiceResponse().getChoices();
                for (AnswerChoice answerChoice : choices) {
                    if (answerChoice.isCorrect() && !answerChoices.contains(answerChoice)) {
                        answerChoices.add(answerChoice);
                    }
                }
            }
        }

        if (answerChoices.isEmpty()) {
            throw new RuntimeException("Insufficient data to generate summary.");
        }

        // can't be empty as we did this check already
        QuestionSession questionSession = questionSessions.get(0);
        List<QuestionChoice> questionSessionChoices = questionSession.getChoices();
        List<QuestionChoice> correctChoices = new ArrayList<>();

        for (AnswerChoice answerChoice : answerChoices) {
            for (QuestionChoice questionChoice : questionSessionChoices) {
                if (answerChoice.getQuestionChoiceId().equals(questionChoice.getId()) && answerChoice.isCorrect()) {
                    if (!correctChoices.contains(questionChoice)) {
                        questionChoice.setCorrect(true);
                        correctChoices.add(questionChoice);
                    }
                    break;
                }
            }
        }

        // Finally we are in a position to generate summary
        // correct question sessions count
        int questionSessionWithAllCorrectChoices = 0;
        for (QuestionSession questionSessionOfLoop : questionSessions) {
            List<AnswerChoice> answerChoicesInLoop = questionSessionOfLoop.getChoiceResponse().getChoices();
            boolean allCorrectSoFar = true;
            for (AnswerChoice answerChoice : answerChoicesInLoop) {
                for (QuestionChoice correctQuestionChoice : correctChoices) {
                    if (correctQuestionChoice.getId().equals(answerChoice.getQuestionChoiceId())
                            && !answerChoice.isSelected()) {
                        // this means one of the choice chosen by user is correct
                        allCorrectSoFar = false;
                    }
                }
            }
            if (allCorrectSoFar) {
                questionSessionWithAllCorrectChoices++;
            }
        }

        // correct question sessions count
        int questionSessionWithAllInCorrectChoices = 0;
        for (QuestionSession questionSessionOfLoop : questionSessions) {
            List<AnswerChoice> answerChoicesInLoop = questionSessionOfLoop.getChoiceResponse().getChoices();
            boolean allInCorrectSoFar = true;
            for (AnswerChoice answerChoice : answerChoicesInLoop) {
                for (QuestionChoice correctQuestionChoice : correctChoices) {
                    if (correctQuestionChoice.getId().equals(answerChoice.getQuestionChoiceId())
                            && answerChoice.isSelected()) {
                        // this means one of the choice chosen by user is correct
                        allInCorrectSoFar = false;
                    }
                }
            }
            if (allInCorrectSoFar) {
                questionSessionWithAllInCorrectChoices++;
            }
        }
        int totalSubmissions = questionSessions.size();
        return QuizSummary.builder()
                .allCorrectCount(questionSessionWithAllInCorrectChoices)
                .incorrectCount(questionSessionWithAllInCorrectChoices)
                .totalSubmissions(totalSubmissions)
                .partiallyCorrectCount(totalSubmissions - (questionSessionWithAllCorrectChoices + questionSessionWithAllInCorrectChoices))
                .correctAnswers(correctChoices)
                .build();
    }
}
