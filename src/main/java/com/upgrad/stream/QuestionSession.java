package com.upgrad.stream;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class QuestionSession {
    private Long id;
    private Long questionId;

    private Long userId;

    private List<QuestionChoice> choices;
    private ChoiceResponseWrapper choiceResponse;

    @JsonProperty("status")
    private AnswerStatus answerStatus;

    @JsonProperty("type")
    private QuestionType questionType;

    @JsonProperty("quiz")
    private Long quizId;

    @JsonProperty("isCorrect")
    private boolean correct;
}