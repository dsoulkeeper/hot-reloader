package com.upgrad.stream;

public enum QuestionType {
    checkbox, radio, numeric
}