package com.upgrad.stream;

import com.google.common.collect.ImmutableList;
import in.ueducation.utils.JsonUtil;
import org.junit.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class SummaryServiceImplTest {

    private final SummaryServiceImpl summaryService = new SummaryServiceImpl();
    public static final String QUIZ_MCQ_RESPONSE_ALL_CORRECT = "{\"id\":6531180,\"questionId\":116311,\"isCorrect\":true,\"isGraded\":true,\"attemptsUsed\":1,\"studentAttemptCount\":1,\"canProceed\":true,\"isEditable\":false,\"isInfinitelyEditable\":false,\"score\":10,\"choiceResponse\":{\"message\":\"\",\"choices\":[{\"id\":306731,\"percentage\":null,\"selected\":false,\"correct\":false,\"feedback\":\"\"},{\"id\":306732,\"percentage\":null,\"selected\":true,\"correct\":true,\"feedback\":\"<p>Extra feedback</p>\"},{\"id\":306733,\"percentage\":null,\"selected\":true,\"correct\":true,\"feedback\":\"\"},{\"id\":306734,\"percentage\":null,\"selected\":false,\"correct\":false,\"feedback\":\"\"}]},\"numericAnswer\":{},\"submissionResponse\":{},\"textAnswer\":{},\"status\":\"graded\",\"feedback\":\"\",\"codeAnswer\":{},\"lastModifiedAt\":1578917309.376371,\"lastModifiedAtTz\":\"2020-01-13T12:08:29.376371+00:00\",\"videoFeedbackId\":null,\"markForReview\":false,\"description\":\"<p>Great question right :D</p>\",\"title\":\"Which characters are part of Avengers\",\"attemptsAllowed\":1,\"choices\":[{\"id\":306731,\"text\":\"<p>Superman</p>\"},{\"id\":306732,\"text\":\"<p>Spiderman</p>\"},{\"id\":306733,\"text\":\"<p>Hulk</p>\"},{\"id\":306734,\"text\":\"<p>Batman</p>\"}],\"submissionConfig\":{},\"textConfig\":{},\"type\":\"checkbox\",\"marks\":[10],\"discussion\":{},\"code\":{},\"order\":1}";
    public static final String QUIZ_MCQ_RESPONSE_ALL_INCORRECT = "{\"id\":6531181,\"questionId\":116311,\"isCorrect\":false,\"isGraded\":true,\"attemptsUsed\":1,\"studentAttemptCount\":1,\"canProceed\":true,\"isEditable\":false,\"isInfinitelyEditable\":false,\"score\":10,\"choiceResponse\":{\"message\":\"\",\"choices\":[{\"id\":306731,\"percentage\":null,\"selected\":true,\"correct\":false,\"feedback\":\"\"},{\"id\":306732,\"percentage\":null,\"selected\":false,\"correct\":false,\"feedback\":\"<p>Extra feedback</p>\"},{\"id\":306733,\"percentage\":null,\"selected\":false,\"correct\":false,\"feedback\":\"\"},{\"id\":306734,\"percentage\":null,\"selected\":true,\"correct\":false,\"feedback\":\"\"}]},\"numericAnswer\":{},\"submissionResponse\":{},\"textAnswer\":{},\"status\":\"graded\",\"feedback\":\"\",\"codeAnswer\":{},\"lastModifiedAt\":1578917309.376371,\"lastModifiedAtTz\":\"2020-01-13T12:08:29.376371+00:00\",\"videoFeedbackId\":null,\"markForReview\":false,\"description\":\"<p>Great question right :D</p>\",\"title\":\"Which characters are part of Avengers\",\"attemptsAllowed\":1,\"choices\":[{\"id\":306731,\"text\":\"<p>Superman</p>\"},{\"id\":306732,\"text\":\"<p>Spiderman</p>\"},{\"id\":306733,\"text\":\"<p>Hulk</p>\"},{\"id\":306734,\"text\":\"<p>Batman</p>\"}],\"submissionConfig\":{},\"textConfig\":{},\"type\":\"checkbox\",\"marks\":[10],\"discussion\":{},\"code\":{},\"order\":1}";
    public static final String QUIZ_MCQ_RESPONSE_PARTIALLY_CORRECT_V1 = "{\"id\":6531183,\"questionId\":116311,\"isCorrect\":false,\"isGraded\":true,\"attemptsUsed\":1,\"studentAttemptCount\":1,\"canProceed\":true,\"isEditable\":false,\"isInfinitelyEditable\":false,\"score\":10,\"choiceResponse\":{\"message\":\"\",\"choices\":[{\"id\":306731,\"percentage\":null,\"selected\":true,\"correct\":false,\"feedback\":\"\"},{\"id\":306732,\"percentage\":null,\"selected\":true,\"correct\":true,\"feedback\":\"<p>Extra feedback</p>\"},{\"id\":306733,\"percentage\":null,\"selected\":false,\"correct\":false,\"feedback\":\"\"},{\"id\":306734,\"percentage\":null,\"selected\":false,\"correct\":false,\"feedback\":\"\"}]},\"numericAnswer\":{},\"submissionResponse\":{},\"textAnswer\":{},\"status\":\"graded\",\"feedback\":\"\",\"codeAnswer\":{},\"lastModifiedAt\":1578917309.376371,\"lastModifiedAtTz\":\"2020-01-13T12:08:29.376371+00:00\",\"videoFeedbackId\":null,\"markForReview\":false,\"description\":\"<p>Great question right :D</p>\",\"title\":\"Which characters are part of Avengers\",\"attemptsAllowed\":1,\"choices\":[{\"id\":306731,\"text\":\"<p>Superman</p>\"},{\"id\":306732,\"text\":\"<p>Spiderman</p>\"},{\"id\":306733,\"text\":\"<p>Hulk</p>\"},{\"id\":306734,\"text\":\"<p>Batman</p>\"}],\"submissionConfig\":{},\"textConfig\":{},\"type\":\"checkbox\",\"marks\":[10],\"discussion\":{},\"code\":{},\"order\":1}";

    public static final String QUIZ_MCQ_RESPONSE_PARTIALLY_CORRECT_V2 = "{\"id\":6531184,\"questionId\":116311,\"isCorrect\":false,\"isGraded\":true,\"attemptsUsed\":1,\"studentAttemptCount\":1,\"canProceed\":true,\"isEditable\":false,\"isInfinitelyEditable\":false,\"score\":10,\"choiceResponse\":{\"message\":\"\",\"choices\":[{\"id\":306731,\"percentage\":null,\"selected\":true,\"correct\":false,\"feedback\":\"\"},{\"id\":306732,\"percentage\":null,\"selected\":false,\"correct\":false,\"feedback\":\"<p>Extra feedback</p>\"},{\"id\":306733,\"percentage\":null,\"selected\":true,\"correct\":true,\"feedback\":\"\"},{\"id\":306734,\"percentage\":null,\"selected\":false,\"correct\":false,\"feedback\":\"\"}]},\"numericAnswer\":{},\"submissionResponse\":{},\"textAnswer\":{},\"status\":\"graded\",\"feedback\":\"\",\"codeAnswer\":{},\"lastModifiedAt\":1578917309.376371,\"lastModifiedAtTz\":\"2020-01-13T12:08:29.376371+00:00\",\"videoFeedbackId\":null,\"markForReview\":false,\"description\":\"<p>Great question right :D</p>\",\"title\":\"Which characters are part of Avengers\",\"attemptsAllowed\":1,\"choices\":[{\"id\":306731,\"text\":\"<p>Superman</p>\"},{\"id\":306732,\"text\":\"<p>Spiderman</p>\"},{\"id\":306733,\"text\":\"<p>Hulk</p>\"},{\"id\":306734,\"text\":\"<p>Batman</p>\"}],\"submissionConfig\":{},\"textConfig\":{},\"type\":\"checkbox\",\"marks\":[10],\"discussion\":{},\"code\":{},\"order\":1}";

    @Test
    public void shouldCalculateSummaryCorrectlyForMCQ() {
        // Arrange
        QuestionSession questionSession1 = JsonUtil.fromJson(QUIZ_MCQ_RESPONSE_ALL_CORRECT, QuestionSession.class);
        QuestionSession questionSession2 = JsonUtil.fromJson(QUIZ_MCQ_RESPONSE_ALL_INCORRECT, QuestionSession.class);
        QuestionSession questionSession3 = JsonUtil.fromJson(QUIZ_MCQ_RESPONSE_PARTIALLY_CORRECT_V1, QuestionSession.class);

        List<QuestionSession> questionSessions = ImmutableList.of(questionSession1, questionSession2, questionSession3);

        // Act
        QuizSummary summarize = summaryService.summarize(questionSessions);

        // Assert
        assertThat(summarize.getAllCorrectCount()).isEqualTo(1);
        assertThat(summarize.getIncorrectCount()).isEqualTo(1);
        assertThat(summarize.getPartiallyCorrectCount()).isEqualTo(1);
        assertThat(summarize.getTotalSubmissions()).isEqualTo(3);
    }

    @Test
    public void shouldCalculateSummaryCorrectlyForMCQWithPartiallyCorrectAnswers() {
        // Arrange
        QuestionSession questionSession1 = JsonUtil.fromJson(QUIZ_MCQ_RESPONSE_PARTIALLY_CORRECT_V1, QuestionSession.class);
        QuestionSession questionSession2 = JsonUtil.fromJson(QUIZ_MCQ_RESPONSE_PARTIALLY_CORRECT_V2, QuestionSession.class);

        List<QuestionSession> questionSessions = ImmutableList.of(questionSession1, questionSession2);

        // Act
        QuizSummary summarize = summaryService.summarize(questionSessions);

        // Assert
        assertThat(summarize.getAllCorrectCount()).isEqualTo(0);
        assertThat(summarize.getIncorrectCount()).isEqualTo(0);
        assertThat(summarize.getPartiallyCorrectCount()).isEqualTo(2);
        assertThat(summarize.getTotalSubmissions()).isEqualTo(2);

        List<QuestionChoice> correctAnswers = summarize.getCorrectAnswers();
        assertThat(correctAnswers.size()).isEqualTo(2);
        assertThat(correctAnswers.get(0).getId()).isEqualTo(306732);
        assertThat(correctAnswers.get(1).getId()).isEqualTo(306733);
    }
}
